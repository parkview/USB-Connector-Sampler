## USB Connector Sampler

This is a KiCAD 5.1 designed PCB that displays a sample of a range of USB connectors that I have used in past projects.  It includes a LCSC part number, symbols, footprints and 3D models.  A bit more information can be found on the SW Maker forum: [SW Makers Forum - USB Connector Sampler](https://forum.swmakers.org/viewtopic.php?f=9&t=2455)

![](images/USB_Connector_Sampler.1_sml.jpg "Example PCB")
 
![](images/PCB_white_sml.jpg "White USB Sampler PCB")
