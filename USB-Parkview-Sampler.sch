EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "USB Connector Sampler"
Date "2021-03-07"
Rev "1.0"
Comp "PRL"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_A J1
U 1 1 60442FFD
P 2470 1070
F 0 "J1" H 2527 1537 50  0000 C CNN
F 1 "USB_A" H 2527 1446 50  0000 C CNN
F 2 "Connector_USB:USB_A_CONNFLY_DS1095-WNR0" H 2620 1020 50  0001 C CNN
F 3 " ~" H 2620 1020 50  0001 C CNN
	1    2470 1070
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B J3
U 1 1 60444096
P 2480 2165
F 0 "J3" H 2537 2632 50  0000 C CNN
F 1 "USB_B" H 2537 2541 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 2630 2115 50  0001 C CNN
F 3 " ~" H 2630 2115 50  0001 C CNN
	1    2480 2165
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J7
U 1 1 60444762
P 2440 4275
F 0 "J7" H 2497 4742 50  0000 C CNN
F 1 "USB_B_Micro" H 2497 4651 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:USB_Micro-B_C10418" H 2590 4225 50  0001 C CNN
F 3 "~" H 2590 4225 50  0001 C CNN
	1    2440 4275
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Plug P2
U 1 1 60445125
P 2405 6130
F 0 "P2" H 2512 7397 50  0000 C CNN
F 1 "USB_C_Plug" H 2512 7306 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:USB_C_Receptacle_C136423" H 2555 6130 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 2555 6130 50  0001 C CNN
	1    2405 6130
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Mini J5
U 1 1 60448F36
P 2440 3260
F 0 "J5" H 2497 3727 50  0000 C CNN
F 1 "USB_B_Mini" H 2497 3636 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Wuerth_65100516121_Horizontal" H 2590 3210 50  0001 C CNN
F 3 "~" H 2590 3210 50  0001 C CNN
	1    2440 3260
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 6044C91B
P 830 800
F 0 "H1" H 930 846 50  0000 L CNN
F 1 "MountingHole" H 930 755 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 830 800 50  0001 C CNN
F 3 "~" H 830 800 50  0001 C CNN
	1    830  800 
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Plug P1
U 1 1 60451ABB
P 5005 6120
F 0 "P1" H 5112 7387 50  0000 C CNN
F 1 "USB_C_Plug" H 5112 7296 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:USB_C_Receptacle_C136423" H 5155 6120 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 5155 6120 50  0001 C CNN
	1    5005 6120
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_A J2
U 1 1 6045465A
P 4970 1100
F 0 "J2" H 5027 1567 50  0000 C CNN
F 1 "USB_A" H 5027 1476 50  0000 C CNN
F 2 "Connector_USB:USB_A_CONNFLY_DS1095-WNR0" H 5120 1050 50  0001 C CNN
F 3 " ~" H 5120 1050 50  0001 C CNN
	1    4970 1100
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B J4
U 1 1 6045485C
P 4980 2195
F 0 "J4" H 5037 2662 50  0000 C CNN
F 1 "USB_B" H 5037 2571 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 5130 2145 50  0001 C CNN
F 3 " ~" H 5130 2145 50  0001 C CNN
	1    4980 2195
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J8
U 1 1 60454866
P 4940 4305
F 0 "J8" H 4997 4772 50  0000 C CNN
F 1 "USB_B_Micro" H 4997 4681 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:USB_Micro-B_C10418" H 5090 4255 50  0001 C CNN
F 3 "~" H 5090 4255 50  0001 C CNN
	1    4940 4305
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Mini J6
U 1 1 60454870
P 4940 3290
F 0 "J6" H 4997 3757 50  0000 C CNN
F 1 "USB_B_Mini" H 4997 3666 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Wuerth_65100516121_Horizontal" H 5090 3240 50  0001 C CNN
F 3 "~" H 5090 3240 50  0001 C CNN
	1    4940 3290
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J9
U 1 1 6045F674
P 7460 4350
F 0 "J9" H 7517 4817 50  0000 C CNN
F 1 "USB_B_Micro" H 7517 4726 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:USB_Micro-B_C319160" H 7610 4300 50  0001 C CNN
F 3 "~" H 7610 4300 50  0001 C CNN
	1    7460 4350
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J10
U 1 1 6046647B
P 8475 4350
F 0 "J10" H 8532 4817 50  0000 C CNN
F 1 "USB_B_Micro" H 8532 4726 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:USB_Micro-B_C319160" H 8625 4300 50  0001 C CNN
F 3 "~" H 8625 4300 50  0001 C CNN
	1    8475 4350
	1    0    0    -1  
$EndComp
Text Notes 1705 1080 0    50   ~ 0
USB 2 A
Text Notes 1720 2140 0    50   ~ 0
USB 2 B
Text Notes 1695 3245 0    50   ~ 0
USB Mini
Text Notes 1620 4255 0    50   ~ 0
USB Micro1
Text Notes 6605 4340 0    50   ~ 0
USB Micro2
Text Notes 1240 6180 0    50   ~ 0
USB C
Text Notes 2505 7695 0    50   ~ 0
Connector
Text Notes 5110 7675 0    50   ~ 0
Footprint
Text Notes 8360 4995 0    50   ~ 0
Footprint
Text Notes 7335 5005 0    50   ~ 0
Connector
NoConn ~ 2770 870 
NoConn ~ 2770 1070
NoConn ~ 2770 1170
NoConn ~ 2470 1470
NoConn ~ 2370 1470
NoConn ~ 5270 900 
NoConn ~ 5270 1100
NoConn ~ 5270 1200
NoConn ~ 4970 1500
NoConn ~ 4870 1500
NoConn ~ 2780 1965
NoConn ~ 2780 2165
NoConn ~ 2780 2265
NoConn ~ 2480 2565
NoConn ~ 2380 2565
NoConn ~ 4880 2595
NoConn ~ 4980 2595
NoConn ~ 5280 1995
NoConn ~ 5280 2195
NoConn ~ 5280 2295
NoConn ~ 4840 3690
NoConn ~ 4940 3690
NoConn ~ 5240 3490
NoConn ~ 5240 3390
NoConn ~ 5240 3290
NoConn ~ 5240 3090
NoConn ~ 2740 3060
NoConn ~ 2740 3260
NoConn ~ 2740 3360
NoConn ~ 2740 3460
NoConn ~ 2440 3660
NoConn ~ 2340 3660
NoConn ~ 2740 4275
NoConn ~ 2740 4375
NoConn ~ 2740 4475
NoConn ~ 2440 4675
NoConn ~ 2340 4675
NoConn ~ 2740 4075
NoConn ~ 5240 4105
NoConn ~ 5240 4305
NoConn ~ 5240 4405
NoConn ~ 5240 4505
NoConn ~ 4940 4705
NoConn ~ 4840 4705
NoConn ~ 5605 5120
NoConn ~ 5605 5320
NoConn ~ 5605 5420
NoConn ~ 5605 5620
NoConn ~ 5605 5820
NoConn ~ 5605 6120
NoConn ~ 5605 6220
NoConn ~ 5605 6420
NoConn ~ 5605 6520
NoConn ~ 5605 6720
NoConn ~ 5605 6820
NoConn ~ 5605 7020
NoConn ~ 5605 7120
NoConn ~ 5605 7320
NoConn ~ 5605 7420
NoConn ~ 5005 7720
NoConn ~ 4705 7720
NoConn ~ 2105 7730
NoConn ~ 2405 7730
NoConn ~ 3005 7430
NoConn ~ 3005 7330
NoConn ~ 3005 7130
NoConn ~ 3005 7030
NoConn ~ 3005 6830
NoConn ~ 3005 6730
NoConn ~ 3005 6530
NoConn ~ 3005 6430
NoConn ~ 3005 6230
NoConn ~ 3005 6130
NoConn ~ 3005 5830
NoConn ~ 3005 5630
NoConn ~ 3005 5430
NoConn ~ 3005 5330
NoConn ~ 3005 5130
NoConn ~ 7760 4150
NoConn ~ 7760 4350
NoConn ~ 7760 4450
NoConn ~ 7760 4550
NoConn ~ 7460 4750
NoConn ~ 7360 4750
NoConn ~ 8475 4750
NoConn ~ 8375 4750
NoConn ~ 8775 4550
NoConn ~ 8775 4450
NoConn ~ 8775 4350
NoConn ~ 8775 4150
Text Notes 925  960  0    50   ~ 0
M4
$EndSCHEMATC
